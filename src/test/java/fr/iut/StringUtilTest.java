package fr.iut;

import junit.framework.TestCase;

import java.util.Locale;

public class StringUtilTest extends TestCase {
    public void testPrettyCurrencyPrint() throws Exception {
        double amount = 65163511.6541;
        String formattedAmount = StringUtil.prettyCurrencyPrint(amount, Locale.FRANCE);
        assertEquals("65\u00A0163\u00A0511,654", formattedAmount);
    }

}