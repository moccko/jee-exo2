package fr.iut;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * A set of String utilities
 */
class StringUtil {
    /**
     * Format a double to a given locale
     *
     * @param amount :  the amount to format
     * @param locale :  the target locale for formatting
     */
    static String prettyCurrencyPrint(final double amount, final Locale locale) {
        return NumberFormat.getInstance(locale).format(amount);
    }
}
