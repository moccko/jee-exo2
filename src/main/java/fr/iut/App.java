package fr.iut;

import java.util.Locale;
import java.util.Scanner;

public class App {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("Type the amount to format : ");
            String input = scanner.nextLine();
            Float amount = Float.valueOf(input);
            String machin = StringUtil.prettyCurrencyPrint(amount, Locale.FRENCH);
            System.out.println(machin);
        }
    }
}
